local L = LibStub("AceLocale-3.0"):NewLocale("SingleWhisp", "enUS", true)

L["SingleWhisp enabled!"] = true
L["Enabled"] = true
L["Enable/disable icon in /who window"] = true
L["Contacted List"] = true
L["List of players contacted"] = true
L["Clear List"] = true
L["Clear list of players contacted"] = true
L["You already contacted that guy."] = true
L["Already in your party."] = true
L["Total: %d"] = true
L["Number of players contacted"] = true
