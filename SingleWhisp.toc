## Interface: 11303
## Version: 0.2.0
## Title: SingleWhisp
## Author: luc2
## Notes: Displays an icon next to player names already contacted in /who window.
## SavedVariables: SingleWhispDB
## OptionalDeps: Ace3
## X-Embeds: Ace3

embeds.xml
enUS.lua
frFR.lua
Main.lua
