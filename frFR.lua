local L = LibStub("AceLocale-3.0"):NewLocale("SingleWhisp", "frFR")

L["SingleWhisp enabled!"] = "SingleWhisp activé!"
L["Enabled"] = "Activé"
L["Enable/disable icon in /who window"] = "Activer/désactiver icône dans fenêtre /who"
L["Contacted List"] = "Liste contactée"
L["List of players contacted"] = "Liste des joueurs contactés"
L["Clear List"] = "Effacer Liste"
L["Clear list of players contacted"] = "Effacer liste des joueurs contactés"
L["You already contacted that guy."] = "Vous avez déjà contacté cet individu."
L["Already in your party."] = "Déjà dans votre groupe."
L["Total: %d"] = "Total : %d"
L["Number of players contacted"] = "Nombre de joueurs contactés"
