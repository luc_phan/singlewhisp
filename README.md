SingleWhisp
===========

Description
-----------

*SingleWhisp* is a *WoW Classic* add-on which displays an icon next to players **already contacted** in the `/who` results:

![Image showing a player already contacted](images/SingleWhisp-already-contacted.png "(French version of WOW Classic)")

In *WOW Classic*, when you are looking for players for a dungeon, you send a message to the *Trade* channel or the *LookingForGroup* channel. If no one answers, you'll start typing:

- `/who 60`
- `/who 59`
- `/who warrior`
- `/who paladin`
- etc.

...and then, you'll contact everyone in the list:

- *Excuse me, sorry to disturb you but I couldn't help but notice that we were from the same faction, and given that we are somehow allies, I was wondering if you'd be interested in an expedition in UBRS with my group?*

...but you don't want to ask them twice!

With this add-on, you will see who you **already whisped**.

Usage
-----

- Type `/singlewhisp` or `/sw` to open the main window.

![Image showing SingleWhisp main window](images/SingleWhisp-window.png "(French version of WOW Classic)")

- Enable the add-on.
- Clear the contacted list.
- Type `/who 60`
- You can start whispering now.

Feature
-------

The add-on also marks players already in your party:

![Image showing a player already in party](images/SingleWhisp-already-member.png "(French version of WOW Classic)")

TODO
----

- ~~combat + /who 60 = lua error ? (cannot reproduce)~~
