-- #############
-- # Constants #
-- #############

local ADDON_NAME = 'SingleWhisp'
local VERSION = 'v0.2.0'

-- ###########
-- # Imports #
-- ###########

SingleWhisp = LibStub("AceAddon-3.0"):NewAddon(ADDON_NAME, "AceConsole-3.0", "AceEvent-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale(ADDON_NAME)
local AceGUI = LibStub("AceGUI-3.0")
local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")

-- ##############
-- # Main Frame #
-- ##############

function SingleWhisp:CreateMainFrame(mainFrameName)
  local MainFrame = AceGUI:Create("Frame")
  MainFrame:SetTitle(ADDON_NAME)
  MainFrame:SetStatusText(ADDON_NAME .. ' ' .. VERSION)
  MainFrame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end) -- releases all widgets -_-
  MainFrame:SetLayout("Flow")
  MainFrame.frame:SetMovable(true)
  MainFrame.frame:SetScript(
    "OnMouseDown",
    function(button)
      MainFrame.frame:StartMoving()
      MainFrame.isMoving = true
    end
  )
  MainFrame.frame:SetScript(
    "OnMouseUp",
    function(button)
      if (MainFrame.isMoving) then
        MainFrame.isMoving = false

        local frame = MainFrame.frame
        frame:StopMovingOrSizing()
        local self = frame.obj
        local status = self.status or self.localstatus
        status.width = frame:GetWidth()
        status.height = frame:GetHeight()
        status.top = frame:GetTop()
        status.left = frame:GetLeft()
      end
    end
  )
  tinsert(UISpecialFrames, mainFrameName) -- allow ESC to close the window
  return MainFrame
end

function SingleWhisp:BuildMainFrame(mainFrameName)
  local MainFrame = self:CreateMainFrame(mainFrameName)
  AceConfigDialog:Open(self.name, MainFrame)
  return MainFrame
end

-- ##########
-- # Add-on #
-- ##########

function SingleWhisp:OnInitialize()
  -- Called when the addon is loaded
  self.name = ADDON_NAME
  self.buttons = {}
  self.realm = GetRealmName()
  SingleWhispMainFrame = nil

  -- declare defaults to be used in the DB
  local defaults = {
    global = {
      enabled = true,
      contactedList = {}
    }
  }
  self.db = LibStub("AceDB-3.0"):New("SingleWhispDB", defaults, true)

  self:InitOptions()
  AceConfigDialog:AddToBlizOptions(self.name, self.name)

  self:RegisterChatCommand("singlewhisp", "ToggleWindow")
  self:RegisterChatCommand("sw", "ToggleWindow")

  self:CreateContactedButtons()
  self:CreateMemberButtons()

  self.Original_WhoList_Update = WhoList_Update
  function WhoList_Update()
    self.Original_WhoList_Update()
    self:WhoList_Update()
  end

  self:RegisterEvent("CHAT_MSG_WHISPER_INFORM")
end

function SingleWhisp:OnEnable()
  -- Called when the addon is enabled
  self:Print(L["SingleWhisp enabled!"])
end

function SingleWhisp:OnDisable()
  -- Called when the addon is disabled
end

function SingleWhisp:ToggleWindow(input)
  if not SingleWhispMainFrame or not SingleWhispMainFrame:IsVisible() then
    self:InitOptions()
    SingleWhispMainFrame = self:BuildMainFrame("SingleWhispMainFrame")
  else
    -- SingleWhispMainFrame:Release()
    SingleWhispMainFrame:Hide()
    -- AceGUI:Release(SingleWhispMainFrame)
    SingleWhispMainFrame = nil
  end
end

function SingleWhisp:SetEnable(info, val)
  self.db.global.enabled = val
  self:WhoList_Update()
end

function SingleWhisp:GetEnable(info)
  return self.db.global.enabled
end

function SingleWhisp:SetContactedList(info, val)
end

function SingleWhisp:GetContactedList(info)
  local keys = self:GetKeys(self.db.global.contactedList)
  local text = ''
  for k, v in pairs(keys) do
    text = text .. '- ' .. v .. '\n'
  end
  return text
end

function SingleWhisp:ClearContacted()
  self.db.global.contactedList = {}
  self:WhoList_Update()
end

function SingleWhisp:GetKeys(tab)
  local keys = {}

  for k, v in pairs(tab) do
    table.insert(keys, k)
  end

  return keys
end

function SingleWhisp:GetMembers()
  local members = {}
  -- local memberCount = GetNumGroupMembers()
  for i = 1, 40 do
    local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(i);
    if name then
      local player, realm = strsplit('-', name, 2)
      -- print(i, name, player, realm)
      members[name] = true
    end
  end
  return members
end

function SingleWhisp:WhoList_Update()
  local members = self:GetMembers()

  local offset = FauxScrollFrame_GetOffset(WhoListScrollFrame)
  for i=1, WHOS_TO_DISPLAY, 1 do
		local index = offset + i

    local player = C_FriendList.GetWhoInfo(index)
    if player then
      local fullName = player.fullName
      local nameWithRealm = fullName .. '-' .. self.realm

      local contactedButtonName = 'SingleWhispContacted' .. i
      local memberButtonName = 'SingleWhispMember' .. i
      local contactedButton = getglobal(contactedButtonName)
      local memberButton = getglobal(memberButtonName)

      contactedButton:Hide()
      memberButton:Hide()

      if self.db.global.enabled then
        if members[fullName] then
          memberButton:Show()
        elseif self.db.global.contactedList[fullName] then
          contactedButton:Show()
        end
      end
    end
	end
end

function SingleWhisp:CreateContactedButtons()
  for i=1, WHOS_TO_DISPLAY, 1 do
    local buttonName = 'SingleWhispContacted' .. i
    local whoButton = getglobal('WhoFrameButton' .. i);
    local nameButton = getglobal('WhoFrameButton' .. i .. 'Name');
    -- local button = CreateFrame("Button", buttonName, whoButton, "UIPanelButtonTemplate")
    local button = CreateFrame("Button", buttonName, whoButton)
    button:SetSize(14, 14)
    button:SetPoint("RIGHT", nameButton)
    button:SetNormalTexture("Interface\\Buttons\\UI-GroupLoot-Pass-Up")
    button:Hide()
    button:SetScript(
      'OnEnter',
      function()
        GameTooltip:SetOwner(button, 'ANCHOR_TOP')
        GameTooltip:SetText(L["You already contacted that guy."])
      end
    )
    button:SetScript(
      'OnLeave',
      function()
        GameTooltip:Hide()
      end
    )
    self.buttons[i] = button
  end
end

function SingleWhisp:CreateMemberButtons()
  for i=1, WHOS_TO_DISPLAY, 1 do
    local buttonName = 'SingleWhispMember' .. i
    local whoButton = getglobal('WhoFrameButton' .. i);
    local nameButton = getglobal('WhoFrameButton' .. i .. 'Name');
    -- local button = CreateFrame("Button", buttonName, whoButton, "UIPanelButtonTemplate")
    local button = CreateFrame("Button", buttonName, whoButton)
    button:SetSize(14, 14)
    button:SetPoint("RIGHT", nameButton)
    button:SetNormalTexture("Interface\\Buttons\\UI-CheckBox-Check")
    button:Hide()
    button:SetScript(
      'OnEnter',
      function()
        GameTooltip:SetOwner(button, 'ANCHOR_TOP')
        GameTooltip:SetText(L["Already in your party."])
      end
    )
    button:SetScript(
      'OnLeave',
      function()
        GameTooltip:Hide()
      end
    )
    self.buttons[i] = button
  end
end

function SingleWhisp:CHAT_MSG_WHISPER_INFORM(event, message, name)
  -- process the event

  if not self.db.global.enabled then
    return
  end

  -- print('SingleWhisp:CHAT_MSG_WHISPER_INFORM()')
  -- print(event, message, name)
  local player, realm = strsplit('-', name, 2)
  -- print(player, realm)
  self.db.global.contactedList[player] = true
  self:WhoList_Update()
end

function SingleWhisp:InitOptions()
  local options = {
    name = self.name,
    handler = self,
    type = 'group',
    args = {
      enable = {
        name = L["Enabled"],
        desc = L["Enable/disable icon in /who window"],
        type = 'toggle',
        set = 'SetEnable',
        get = 'GetEnable',
        order = 1
      },
      contactedList = {
        name = L["Contacted List"],
        desc = L["List of players contacted"],
        type = 'input',
        multiline = 20,
        width = 'full',
        set = 'SetContactedList',
        get = 'GetContactedList',
        order = 2
      },
      total = {
        type = 'description',
        name = function()
          -- return string.format(L["Total: %d"], table.getn(self.db.global.contactedList))
          return string.format(L["Total: %d"], self:GetLength(self.db.global.contactedList))
        end,
        desc = L["Number of players contacted"],
        order = 3
      },
      clear = {
        name = L["Clear List"],
        desc = L["Clear list of players contacted"],
        type = 'execute',
        func = 'ClearContacted',
        order = 4
      },
    },
  }

  AceConfig:RegisterOptionsTable(self.name, options)
end

function SingleWhisp:GetLength(t)
  local k, v, n
  n = 0
  for k, v in pairs(self.db.global.contactedList) do
    -- print(k, v)
    n = n + 1
  end
  -- print(#self.db.global.contactedList)
  -- print(table.getn(self.db.global.contactedList))
  return n
end
